To add Folder in Laravel:

-dockerize
	-app
		addFile: php.dockerfile
	-mysql
		-data
		addFile: my.cnf
	-nginx
		addFile: default.conf

To add file in root folder:

-docker-compose.yml
-Dockerfile	

------------------------------------------------------------------------------------
Files:

php.dockerfile
	
	FROM php:7.4-fpm-alpine

	WORKDIR /var/www/html

	RUN docker-php-ext-install pdo pdo_mysql

----------------------------------------------------

default.conf
	
	server {
	    listen 80;
	    index index.php index.html;
	    server_name localhost;
	    error_log  /var/log/nginx/error.log;
	    access_log /var/log/nginx/access.log;
	    root /var/www/html/public;

	    location / {
	        try_files $uri $uri/ /index.php?$query_string;
	    }

	    location ~ \.php$ {
	        try_files $uri =404;
	        fastcgi_split_path_info ^(.+\.php)(/.+)$;
	        fastcgi_pass php:9000;
	        fastcgi_index index.php;
	        include fastcgi_params;
	        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
	        fastcgi_param PATH_INFO $fastcgi_path_info;
	    }
	}

---------------------------------------------------------

docker-compose.yml
	
	version: "3.8"

	services:

	  #Nginx Service
	  webserver:
	    image: nginx:stable-alpine
	    container_name: lcp_nginx
	    restart: unless-stopped
	    ports:
	      - "8080:80"
	    volumes: 
	      - .:/var/www/html
	      - ./dockerize/nginx/default.conf:/etc/nginx/conf.d/default.conf
	    depends_on: 
	      - php
	    networks:
	      - laraveldockerize

	  #PHP Service
	  php:
	    build:
	      context: .
	      dockerfile: ./dockerize/app/php.dockerfile
	    container_name: lcp_php
	    volumes: 
	      - .:/var/www/html
	    ports: 
	      - "9000:9000"
	    networks:
	      - laraveldockerize 

	  #Composer Service
	  composer:
	    image: composer:latest
	    container_name: lcp_composer
	    volumes: 
	      - .:/var/www/html
	    working_dir: /var/www/html
	    depends_on: 
	      - php
	    networks:
	      - laraveldockerize    

	  #NPM Service
	  npm:
	    image: node:14.9
	    container_name: lcp_npm
	    volumes: 
	      - .:/var/www/html
	    working_dir: /var/www/html
	    entrypoint: ['npm']

	  #Artisan Service
	  artisan:
	    build: 
	      context: .
	      dockerfile: ./dockerize/app/php.dockerfile
	    container_name: lcp_artisan
	    volumes: 
	      - .:/var/www/html
	    working_dir: /var/www/html
	    entrypoint: ['php', '/var/www/html/artisan']
	    networks:
	      - laraveldockerize


	#Docker Networks
	networks:
	  laraveldockerize:

---------------------------------------------------------------

Dockerfile
	
	FROM webdevops/php-nginx:7.4-alpine

	# Install Laravel framework system requirements (https://laravel.com/docs/8.x/deployment#optimizing-configuration-loading)
	RUN apk add oniguruma-dev postgresql-dev libxml2-dev
	RUN docker-php-ext-install \
	        bcmath \
	        ctype \
	        fileinfo \
	        json \
	        mbstring \
	        pdo_mysql \
	        pdo_pgsql \
	        tokenizer \
	        xml

	# Copy Composer binary from the Composer official Docker image
	COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

	ENV WEB_DOCUMENT_ROOT /app/public
	ENV APP_ENV production
	WORKDIR /app
	COPY . .

	RUN composer install --no-interaction --optimize-autoloader --no-dev
	# Optimizing Configuration loading
	RUN php artisan config:cache
	# Optimizing Route loading
	RUN php artisan route:cache
	# Optimizing View loading
	RUN php artisan route:cache

	RUN chown -R application:application .

