<!-- 1. Laravel -->
Step 1: composer create-project laravel/laravel "name"
Step 2: Add Folder and Files 


<!-- 2. Laravel to Docker -->
Step 1: docker-compose build
Step 2: docker-compose up -d




<!-- 3. Docker to ECR -->
Step 1: Create Repository
Step 2: Select "Private"
Step 3: Add Repo Name "name"
Step 4: Click "Create repository"
Step 5: Go to your ECR repo
Step 6: *aws configure
Step 7: Click "View push commands"
Step 8: [view push command] paste to CLI

		<!-- *AWS CONFIGURE -->
		Step 1: Go to IAM
		Step 2: Go to Users
		Step 3: Select your User
		Step 4: Go to "Security credential"
		Step 5: Create access key



<!-- 4. ECR to ECS -->

<!-- 2. Target Group -->
Step 1: Create target group
Step 2: Choose Instance
Step 3: Add TG name
Step 4: Create target name


<!-- 5. Auto Load Balancer -->
Step 1: Create ALB
Step 2: Select ALB
Step 3: Add ALB name
Step 4: Select all Mapping
Step 5: Create new security group
Step 6: Select your target group
Step 7: Then Create


<!-- 6. Creating Cluster -->
Step 1: Create Cluster
Step 2: Select EC2 Linux + Networking
Step 3: Add Cluster name "apf-sample"
Step 4: Select "On-Demand Instance"
Step 5: Select t2.micro (EC2 instance type)
Step 6: Select available VPC
Step 7: Select all Available subnet
Step 8: Select your SG

<!-- 7. Task Definitions -->
Step 1: Create task Definition
Step 2: Select EC2
Step 3: Add definition name
Step 4: select ecsTask.... (Task role)
Step 5: Select None (Network Mode)
Step 6: Click Add Container
Step 7: Then Click "Create"

	<!-- 8. Add container -->
	Step 1: Add Container name
	Step 2: Get URI image to your ECR
	Step 3: 128 (Memory Limits)
	Step 4: Add key From .env file
	Step 5: Then Add
 

<!-- 9. Service -->
Step 1: Create Service
Step 2: Select EC2
Step 3: Select your task definition
Step 4: latest revision
Step 5: cluster name same with the selected task definition
Step 6: set 1 (number of tasks)
Step 7: NEXT STEP
Step 8: Select "Application Load Balancer"
Step 9: Select your created ALB
Step 10: click Add to load balancer
Step 11: 80:HTTP (Production listener port)
Step 12: Select your created target group (target group name)
Step 13: Next Step
Step 14: Next step
Step 15: Create Service


<!-- 10. To View  -->
Step 1: Go to Load Balancer
Step 2: Copy the DNS 
Step 3: Paste to browser

